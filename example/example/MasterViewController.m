//
//  MasterViewController.m
//  example
//
//  Created by Lu Minh Nha on 7/24/15.
//  Copyright (c) 2015 ISB Viet Nam. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"

@interface MasterViewController ()

@property NSMutableArray *objects;
@end

@implementation MasterViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender {
    if (!self.objects) {
        self.objects = [[NSMutableArray alloc] init];
    }
    [self.objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = self.objects[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    NSDate *object = self.objects[indexPath.row];
    cell.textLabel.text = [object description];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}



/********************************************
 Description: Parse user message to JSON by using Regurlar Expression and google search
 Input: String messgae
 Output: String JSON
 ********************************************/

-(NSString*)parseMyMessageWithString:(NSString*)message
{
    //Array contains mentions
    NSMutableArray* mentionArr = [NSMutableArray array];
    //Array contains emotion
    NSMutableArray* emotionArr = [NSMutableArray array];
    //Dictionary contains url
    NSMutableDictionary* urlDict = [NSMutableDictionary dictionary];
    //Output dictionary
    NSMutableDictionary* finalDict = [NSMutableDictionary dictionary];
    
    //pattern for word
    NSString* mentionPattern = @"@(\\w+)";
    //patter for alphanumeric characters
    NSString* emotionPattern = @"\\(([a-zA-Z0-9_]+)\\)";
    //Array of patterns
    NSMutableArray* patternArr = [NSMutableArray arrayWithObjects:mentionPattern, emotionPattern, nil];
    
    //Use Regular Expression to parse user's message
    for (NSString* pattern in patternArr) {
        NSError * error = nil;
        NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionDotMatchesLineSeparators error:&error];
        
        if (NULL == error) {
            NSArray *matches = [regularExpression matchesInString:message options:0 range:NSMakeRange(0, message.length)];
            
            for (NSTextCheckingResult *match in matches) {
                NSString* resultText = [message substringWithRange:[match rangeAtIndex:1]];
                
                NSInteger index = -1;
                index = [patternArr indexOfObject:pattern];
                
                switch (index) {
                    case 0:
                        //Get mention characters
                        [mentionArr addObject:resultText];
                        break;
                    case 1:
                        //Get emotion characters
                        if (resultText.length <= 15) {
                            [emotionArr addObject:resultText];
                        }
                        break;
                    default:
                        //Error
                        NSLog(@"Error for pattern index: %d", index);
                        break;
                }
                
            }
        }
        else
        {
            NSLog(@"Regular expression error: %@", error);
        }
    }
    @try {
        
        //Search Google with keyword is the user's message, get the first result
        //Skip searching of message is empty
        if (message && ![message isEqualToString:@""]) {
            //Keyword is user's message
            NSString* query = message;
            query = [query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            //Create searching url
            NSString *tempString = [NSString stringWithFormat:@"%@/%@", @"https://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=", query];
            
            NSURL *url = [NSURL URLWithString:tempString];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            NSError *requestError;
            NSURLResponse *urlResponse = nil;
            //Send searching request
            NSData *data =
            [NSURLConnection sendSynchronousRequest:request
                                  returningResponse:&urlResponse error:&requestError];
            if (data && !requestError) {
                //Return result with no error
                NSError *parseError = nil;
                NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                if (dictionary && !parseError) {
                    //No parsing eror
                    NSDictionary* resultDict = [dictionary objectForKey:@"responseData"];
                    if (resultDict) {
                        NSArray* resultArr = [resultDict objectForKey:@"results"];
                        if (resultArr && resultArr.count > 0) {
                            //Get the first result
                            NSDictionary* resultLinkDict = [resultArr firstObject];
                            if (resultLinkDict) {
                                //Get title and url
                                NSString* url = [[resultLinkDict objectForKey:@"url"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                NSString* title = [resultLinkDict objectForKey:@"titleNoFormatting"];
                                [urlDict setValue:url forKey:@"url"];
                                [urlDict setValue:title forKey:@"title"];
                            }
                        }
                    }
                }
                else
                {
                    NSLog(@"Parse error");
                }
            }
            else
            {
                NSLog(@"Search Google error");
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@", exception);
    }
    @finally {
        //Nothing to do with ARC
    }
    
    
    //Build final JSON
    [finalDict setObject:mentionArr forKey:@"mentions"];
    [finalDict setObject:emotionArr forKey:@"emoticons"];
    [finalDict setObject:urlDict forKey:@"links"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:finalDict
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //Return the output JSON string
        return jsonString;
    }

    
    return nil;
}

@end
