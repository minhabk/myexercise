//
//  AppDelegate.h
//  example
//
//  Created by Lu Minh Nha on 7/24/15.
//  Copyright (c) 2015 ISB Viet Nam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

