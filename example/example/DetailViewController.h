//
//  DetailViewController.h
//  example
//
//  Created by Lu Minh Nha on 7/24/15.
//  Copyright (c) 2015 ISB Viet Nam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

