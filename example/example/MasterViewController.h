//
//  MasterViewController.h
//  example
//
//  Created by Lu Minh Nha on 7/24/15.
//  Copyright (c) 2015 ISB Viet Nam. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController
-(NSString*)parseMyMessageWithString:(NSString*)message;
@property (strong, nonatomic) DetailViewController *detailViewController;


@end

