//
//  testcase01_example.m
//  example
//
//  Created by Lu Minh Nha on 7/27/15.
//  Copyright (c) 2015 ISB Viet Nam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "MasterViewController.h"

@interface testcase01_example : XCTestCase
@property (nonatomic) MasterViewController* viewController;
@end
@implementation testcase01_example
- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

/*******************************************************
 * Purpose: Test empty message
 * Description: input an empty message
 * Expected result: return empty JSON string
 *******************************************************/

- (void)testcase001 {
    //Input message
    NSString* inputMessage = @"";
    
    //Initialize input elements
    NSArray* mentionArr = [NSArray array];
    NSArray* emotionIconArr = [NSArray array];
    NSMutableDictionary* urlDict = [NSMutableDictionary dictionary];
    
    //Initilize expected result dictionary
    NSMutableDictionary* expectedDictionary = [NSMutableDictionary dictionary];
    [expectedDictionary setObject:mentionArr forKey:@"mentions"];
    [expectedDictionary setObject:emotionIconArr forKey:@"emoticons"];
    [expectedDictionary setObject:urlDict forKey:@"links"];
    
    //Create expected JSON data
    NSError *error = nil;
    NSData *expectedData = [NSJSONSerialization dataWithJSONObject:expectedDictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    //Create expected JSON string
    NSString* expectedResult = [[NSString alloc] initWithData:expectedData encoding:NSUTF8StringEncoding];
    
    //Call function to parse input message
    self.viewController = [[MasterViewController alloc] init];
    //Get output string from parsing function
    NSString* output = [self.viewController parseMyMessageWithString:inputMessage];
    
    //Compare the output and expected result
    XCTAssertEqualObjects(output, expectedResult);
}



/*******************************************************
 * Purpose: Test metion
 * Description: input a message with 2 mentions: chris_ & jane, both contain words
 * Expected result: 2 mentions are detected: chris_ & jane, empty emotion, a corresponding url
 * The expected url is firgured out by searching google with keyword is input message
 *******************************************************/

- (void)testcase002 {
    //Input message
    NSString* inputMessage = @"@chris_ @jane you around?";
    
    //Initialize input elements
    NSArray* mentionArr = [NSArray arrayWithObjects:@"chris_", @"jane", nil];
    NSArray* emotionIconArr = [NSArray array];
    
    //Initialize expected url information
    NSMutableDictionary* urlDict = [NSMutableDictionary dictionary];
    [urlDict setValue:@"Chris Brown - See You Around - YouTube" forKey:@"title"];
    [urlDict setValue:@"http://www.youtube.com/watch?v=20et_Y9b4_M" forKey:@"url"];
    
    //Initilize expected result dictionary
    NSMutableDictionary* expectedDictionary = [NSMutableDictionary dictionary];
    [expectedDictionary setObject:mentionArr forKey:@"mentions"];
    [expectedDictionary setObject:emotionIconArr forKey:@"emoticons"];
    [expectedDictionary setObject:urlDict forKey:@"links"];
    
    //Create expected JSON data
    NSError *error = nil;
    NSData *expectedData = [NSJSONSerialization dataWithJSONObject:expectedDictionary
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
    //Create expected JSON string
    NSString* expectedResult = [[NSString alloc] initWithData:expectedData encoding:NSUTF8StringEncoding];
    
    //Call function to parse input message
    self.viewController = [[MasterViewController alloc] init];
    //Get output string from parsing function
    NSString* output = [self.viewController parseMyMessageWithString:inputMessage];
    
    //Compare the output and expected result
    XCTAssertEqualObjects(output, expectedResult);
}




/*******************************************************
 * Purpose: Test emotion
 * Description: input a message with 4 emotions: megusta32, coffee21, 1234567890123456, 123456789012345
 * Expected result: 3 emotions are detected: megusta32 ,coffee21 & 123456789012345, emotion 1234567890123456 is rejected because it exceeds maximum length (15 characters), empty mention, empty url
 *******************************************************/

-(void)testcase003
{
    //Input message
    NSString* inputMessage = @"Good morning! (megusta32) (coffee21) (1234567890123456) (123456789012345)";
    
    //Initialize input elements
    NSArray* mentionArr = [NSArray array];
    NSArray* emotionIconArr = [NSArray arrayWithObjects:@"megusta32", @"coffee21", @"123456789012345",nil];
    NSMutableDictionary* urlDict = [NSMutableDictionary dictionary];
    
    //Initilize expected result dictionary
    NSMutableDictionary* expectedDictionary = [NSMutableDictionary dictionary];
    [expectedDictionary setObject:mentionArr forKey:@"mentions"];
    [expectedDictionary setObject:emotionIconArr forKey:@"emoticons"];
    [expectedDictionary setObject:urlDict forKey:@"links"];
    
    //Create expected JSON data
    NSError *error = nil;
    NSData *expectedData = [NSJSONSerialization dataWithJSONObject:expectedDictionary
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
    //Create expected JSON string
    NSString* expectedResult = [[NSString alloc] initWithData:expectedData encoding:NSUTF8StringEncoding];
    
    //Call function to parse input message
    self.viewController = [[MasterViewController alloc] init];
    //Get output string from parsing function
    NSString* output = [self.viewController parseMyMessageWithString:inputMessage];
    
    //Compare the output and expected result
    XCTAssertEqualObjects(output, expectedResult);
}



/*******************************************************
 * Purpose: Test all features
 * Description: input a message with 1 mention: gmail, 1 emotion: PR
 * Expected result: 1 mention is detected: gmail, 1 emotion is detected: PR, 1 corresponding url
 * The expected url is firgured out by searching google with keyword is input message
 *******************************************************/

-(void)testcase004
{
    //Input message
    NSString* inputMessage = @"Plum.vn ISB Appstore (PR) @gmail";
    
    //Initialize input elements
    NSArray* mentionArr = [NSArray arrayWithObject:@"gmail"];
    NSArray* emotionIconArr = [NSArray arrayWithObject:@"PR"];
    NSMutableDictionary* urlDict = [NSMutableDictionary dictionary];
    
    //Initialize expected url information
    [urlDict setValue:@"Electrical, Electronic and Cybernetic Brand Name Index" forKey:@"title"];
    [urlDict setValue:@"http://www.wolfbane.com/tv.htm" forKey:@"url"];
    
    //Initilize expected result dictionary
    NSMutableDictionary* expectedDictionary = [NSMutableDictionary dictionary];
    [expectedDictionary setObject:mentionArr forKey:@"mentions"];
    [expectedDictionary setObject:emotionIconArr forKey:@"emoticons"];
    [expectedDictionary setObject:urlDict forKey:@"links"];
    
    //Create expected JSON data
    NSError *error = nil;
    NSData *expectedData = [NSJSONSerialization dataWithJSONObject:expectedDictionary
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
    //Create expected JSON string
    NSString* expectedResult = [[NSString alloc] initWithData:expectedData encoding:NSUTF8StringEncoding];
    
    //Call function to parse input message
    self.viewController = [[MasterViewController alloc] init];
    NSString* output = [self.viewController parseMyMessageWithString:inputMessage];
    //Get output string from parsing function
    
    //Compare the output and expected result
    XCTAssertEqualObjects(output, expectedResult);
}



- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
