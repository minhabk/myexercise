*************************************************************************************
*                                 README.txt                                        *      
*************************************************************************************

- The project was executed normally with Xcode 6.4, MAC OS 10.10.4 (64 bits)
- The parse function is located in class "MasterViewController" with name "-(NSString*)parseMyMessageWithString:(NSString*)message" 
(in directory: "./example/"")
- The testcases is located in class "testcase01_example", contains 4 testcases (in directory: "./exampleTests/"")

*************************************************************************************
*                                 How to use                                        *      
*************************************************************************************
- Call function -(NSString*)parseMyMessageWithString:(NSString*)message in class "MasterViewController" 
and input a message you want to parse, the return message is required JSON message.
- Please execute one test case at once, the return url is firgured out by search google with keyword is the input message.